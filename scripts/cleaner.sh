#!/bin/zsh

declare -A arr

IFS=
for filename in ./*.pcap; do
	index=`tail -n1  $filename | awk -F '//file' '{print $2}'`
	arr[$index]=$(cat $filename)

done

echo "" > main.c
for i in {1..760}; do
	#echo ${arr[$i]} >> main.c
	printf '%s\n' ${arr[$i]} >> main.c
done

echo "==== EXECUTION ===="
gcc main.c -o cleaner
output=`./cleaner`
echo $output
rm cleaner
echo "======= END ======="
password=`echo $output | grep "IS" | awk -F ': ' '{print $2}'`
echo -n $password | openssl sha -sha256 | cut -d ' ' -f2
echo "You can remove main.c now"
